<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * @ORM\Entity
  * @ORM\Table(name="genus")
 */
class Genus
{
    /**
      * @ORM\Id
      * @ORM\GeneratedValue(strategy="AUTO")
      * @ORM\Column(type="integer")
     */
     private $id;

     /**
       * @ORM\Column(type="string")
      */
     private $name;

     public function getName()
     {
         return $this->name;
     }

     public function setName($name)
     {
         $this->name = $name;
     }

     /**
       * @ORM\Column(type="string")
      */
     private $nameHref;

     public function getNameHref()
     {
         return $this->nameHref;
     }

     public function setNameHref($nameHref)
     {
         $this->nameHref = $nameHref;
     }
}
