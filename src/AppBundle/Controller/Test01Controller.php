<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class Test01Controller extends Controller
{
    /**
     * @Route("/test", name="homepage")
     */
    public function test01Action(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('test01/index.html.twig');
    }


    /**
     * @Route("about", name="about")
     */
    public function testAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('test01/ab.html.twig');
    }

    /**
     * @Route("liczba", name="numberRandom")
     */
    public function losujLiczbeAction(Request $request)
    {
        $wylosowanaLiczba = rand(15,75);
        return $this->render('test01/szczesliwaLiczba.html.twig', array('szczesliwaLiczba' => $wylosowanaLiczba));
    }
}
