<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Genus;

class GenusController extends Controller
{
        /**
          *@Route("/genus/{genusName}")
          */

    public function jaksNazwaFunkcji($genusName)
    {
        $funFact = 'Octopuses can change the color of their body in just *three-tenths* of a second!';
        $funFact = $this->get('markdown.parser')->transform($funFact);
        return $this->render('genus/show.html.twig', [
                'name'=>$genusName,
                'funFact'=>$funFact
                ]
            );
    }


    /**
      *@Route("/genus/{genusName}/notes", name="genus_show_notes")
      *@Method("GET")
    */
    public function getNotesAction($genusName)
    {

        $notes = [
            ['id' => 1, 'username' => 'AquaPelham', 'avatarUri' => '/images/leanna.jpeg', 'note' => 'Octopus asked me a riddle, outsmarted me', 'date' => 'Dec. 10, 2015'],
            ['id' => 2, 'username' => 'AquaWeaver', 'avatarUri' => '/images/ryan.jpeg', 'note' => 'I counted 8 legs... as they wrapped around me', 'date' => 'Dec. 1, 2015'],
            ['id' => 3, 'username' => 'AquaPelham', 'avatarUri' => '/images/leanna.jpeg', 'note' => 'Inked!', 'date' => 'Aug. 20, 2015'],
        ];

        $data = [
            'notes' => $notes
        ];

         return new JsonResponse($data);
    }


    /**
      *@Route("/wrzucdobazy")
      */
    public function zaladujDaneDoBazy()
    {
        $genus = new Genus();
        $genus->setName('Ośmiornica'.rand(1,200));
        $genus->setNameHref('Ośmiornica'.rand(1,200));
        $em=$this->getDoctrine()->getManager();
        $em->persist($genus);
        $em->flush();
        return new Response("<html><body><i>obiekt załadowano do bazy</i><body></html>");
    }


    /**
      *@Route("/ladujdane")
      */
    public function ladujDane()
    {
        $dane = array("http://asteroid150.ayz.pl/moon/", "moon",
                    "http://asteroid150.ayz.pl/warp/", "warp drive",
                    "http://asteroid150.ayz.pl/asteroid150czyde/asczyde01.html", "asteroid 3",
                    "http://asteroid150.ayz.pl/asteroid150alfa/asteroid150.html", "asteroid 150");


        for ($i = 0; $i < 4; $i++) {

            $genus = new Genus();
            $genus->setName($dane[$i*2]);
            $genus->setNameHref($dane[$i*2+1]);
            $em=$this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();
        }

        return new Response("<html><body><i>obiekt załadowano do bazy</i><body></html>");
    }



    /**
      *@Route("/")
      */
    public function listAction()
   {
       $em = $this->getDoctrine()->getManager();
       $genuses = $em->getRepository('AppBundle:Genus')
           ->findAll();

       return $this->render('genus/list.html.twig', [
        'genuses' => $genuses
        ]);
   }
}
